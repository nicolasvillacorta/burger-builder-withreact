import React, { Component } from 'react';
import Modal from '../../components/UI/Modal/Modal';
import Aux from '../Auxiliary/Auxiliary';

/*
    Lo hago en minuscula, porque lo llamo en el burgerbuilder, NO como componente, sino como una funcion que wrappea el export del burgerBuilder, abajo de todo.
*/

const withErrorHandler = (WrappedComponent, axios) => {

    return class extends Component {
        
        state = {
            error: null
        }

        //componentDidMount() {  Tuve que comentarlo y hacer el componentWillMount, porque por la teoria de ciclos de vida, el didMount no iba a mostrarme nunca 
        // ni los ingredientes ni el error, cuando hace el get inicial de ingredientes.
        componentWillMount() {
           
            this.reqInterceptor = axios.interceptors.request.use( req => {
                this.setState({error: null});
                return req;
            })
            
            this.resInterceptor =  axios.interceptors.response.use(res => res, (error => {
                this.setState({error: error});
            }));
        }

        // Cuando empiece a usar Routing, los interceptores se van a empezar a instanciar e instanciar cada vez que se monte un withErrorHandler
        // (por el BurgerBuilder) y van a llenar la memoria.
        // Este lifecycle se ejecuta cuando el componente no se necesita mas.
        componentWillUnmount() {
            // console.log('Will unmount', this.reqInterceptor, this.resInterceptor); [ESTA LINEA LA USE NOMAS PARA PROBAR QUE FUNCIONABA EL WILLUNMOUNT]
            // Asigno los interceptors como variables en el willMount asi despues aca puedo referenciarlos y removerlos.
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor); 
            // Y asi remuevo los interceptors para evitar memory leaks.
        }

        errorConfirmedHandler = () => {
            this.setState({error:null})
        }

        render(){
            return(
                <Aux>
                    <Modal 
                        show={this.state.error}
                        modalClosed={this.errorConfirmedHandler} >
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent  {...this.props}/>
                </Aux>
            );
        }
    }
}

export default withErrorHandler;