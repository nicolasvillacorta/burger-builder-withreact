import React, { Component } from 'react';
import Aux from '../Auxiliary/Auxiliary';
import styles from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

/*
    En un comienzo lo tenia en la carpeta components, al final lo movi al hoc, ya que solo es un componente hecho para wrappear otros, igual que el Aux.  
*/
class Layout extends Component {

    state = {
        showSideDrawer: false
    }

    sideDrawerTogglerHandler = () => {
        /* this.setState({showSideDrawer: !this.state.showSideDrawer}); -> Es recomendable no setear el estado de esta forma, puede llevar a comportamiento indeseado, si de la siguiente linea. */
        this.setState((prevState) => {
           
            return { showSideDrawer: !prevState.showSideDrawer}
            
        });
    }

    sideDrawerClosedHandler = () => {
        this.setState({showSideDrawer: false});
    }

    render(){
      return (  
        <Aux>
            <Toolbar drawerToggleClicked={this.sideDrawerTogglerHandler}/>
            <SideDrawer 
                open={this.state.showSideDrawer} 
                closed={this.sideDrawerClosedHandler} />
            <main className={styles.Content}>
                {this.props.children}
            </main>
        </Aux>)
    }
};

export default Layout;