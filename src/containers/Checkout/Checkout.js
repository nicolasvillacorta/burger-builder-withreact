import React, { Component } from 'react'
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import ContactData from '../ContactData/ContactData';

class Checkout extends Component {
   
    // componentDidMount() { Lo comento porque sino le estoy pasando apenas se monta los ingrdientes null y rompe.
    /*componentWillMount() {

        const query = new URLSearchParams(this.props.location.search);
        const ingredients = {};
        let price = 0;

        for(let param of query.entries()){
            // ['salad', '1']
            if(param[0] === 'price'){
                price = param[1];         
            } else {
                ingredients[param[0]] = +param[1];
            }
        }

        this.setState({ingredients: ingredients, totalPrice: price});
    } [CON EL AGREGADO DE REDUX YA NO NECESITO ESTE METODO PARA QUE LEVANTE LOS QUERYPARAMS. TAMBIEN BORRE EL STATE. ]*/

    checkoutCancelledHandler = () => {
        this.props.history.goBack();
    }

    checkoutContinuedHandler = () => {
        this.props.history.replace("/checkout/contact-data")
    }

    render(){
        
        return (
            <div>
                <CheckoutSummary 
                    ingredients={this.props.ings}
                    checkoutCancelled={this.checkoutCancelledHandler}
                    checkoutContinued={this.checkoutContinuedHandler} />
                <Route 
                    path={this.props.match.path + '/contact-data'} 
                    component={ContactData} /> {/* Al final termine usando el component, porque como use redux, no necesite para nada el price  en este componente. */}
                    {/* render={ (props) => <ContactData ingredients={this.state.ingredients} price={this.props.price} {...props} /> 
                       component={ContactData} [YO QUIERO PASARLE LAS PROPS, POR ESO TENGO QUE USAR EL RENDER EN LUGAR DEL COMPONENT.]*/} 
                   
                    {/* Si al render no le paso las props, por estar usando el render, no va a tener las props del router el componente, para eso se las paso
                        como hice en este ejemplo, o tambien podria wrappear el ContactData con el withRouter component.*/}

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients
    }
}

export default connect(mapStateToProps)(Checkout);