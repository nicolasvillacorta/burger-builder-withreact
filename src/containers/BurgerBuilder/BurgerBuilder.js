import React, { Component } from "react";
import { connect } from 'react-redux';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';
import * as actionType from '../../store/actions';

class BurgerBuilder extends Component {

    //constructor(props) {
    //    super(props); ESTA SINTAXIS SE USABA ANTES, ES VIABLE AHORA, PERO SE USA EL STATE = {..} QUE ES MAS FACIL
    //    this.state = {...}
    //}
    state = {
        /*ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        }, COMENTE ESTA LISTA QUE ARRANCABA EN 0 Y LE PUSE NULL, PARA TRAERMELA EN EL COMPONENTEDIDMOUNT DE LA BASE DE DATOS.
        Arrancando de null, me tira una nullpointer la funciona que mapea los ingredients, asi que voy a agregar un spinner en lugar del burger
        y en lugar del burgercontrol algun cartel que diga que estoy esperando los ingredientes.*/
        purchasing: false,
        loading: false,
        error: false
        // Al final con el redux, solo deje aca el state necesario para los cambios de UI. Los demas de la app estan en el store.
    }

    componentDidMount() {

        //axios.get('/ingredients.json')
        //    .then(response => {
        //        this.setState({ingredients: response.data})
        //    })
        //    .catch(error => {
        //        this.setState({error: true})
        //    })
    }

    updatePurchaseState(ingredients){
        
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey]
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        // this.setState({purchaseable: sum > 0}) Asi lo usaba antes de tener redux, ahora, podria dejar un state que sea purchasing en redux, o convertir esto en retorno bool y que lo pase al componente hijo.
            return sum > 0;
    }

    // USANDO LA ARROW FUNCTION, "this" referencia a la clase BurgerBuilder.
    // Si uso un metodo normal, no funciona igual que en la OOP ordinaria. (Nose bien a que referencia.)
    // SIEMPRE los eventos se tienen que lanzar con funciones flecha.
    purchaseHandler = () => {
        this.setState({purchasing: true})
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false})
    }

    purchaseContinueHandler = () => {
        /* [COMENTO TODA ESTA PARTE; PORQUE AHORA VOY A USAR REDUX.]
            const queryParams = [];
            for(let i in this.state.ingredients){
                queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
            }
            queryParams.push('price=' + this.state.totalPrice);
            const queryString = queryParams.join('&');;
        
            this.props.history.push({
                pathname: '/checkout',
                search: '?' + queryString
            });
        */
       this.props.history.push('/checkout');
    }

    render(){
        
        const disabledInfo = {
             ...this.props.ings
        };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0
        } 
        
        let orderSummary = null;
        // Por default la burger es un spinner, si ya tengo ingredientes pasa a ser el Burger y BuildControls components.
        let burger = this.state.error ? <p>Ingredientes can't be loaded!</p> :  <Spinner />; 

        if(this.props.ings) {
            burger = (
                <Aux>
                    <Burger ingredients={this.props.ings} />
                    <BuildControls 
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabledInfo} 
                        purchaseable={this.updatePurchaseState(this.props.ings)}
                        ordered={this.purchaseHandler}
                        price={this.props.price} />
                </Aux>
            );
            orderSummary = <OrderSummary 
                price={this.props.price}
                purchaseContinued={this.purchaseContinueHandler}
                purchaseCancelled={this.purchaseCancelHandler}
                ingredients={this.props.ings} />
        }
        if(this.state.loading){
            orderSummary = <Spinner />
        }
       

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler} >
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

// Los prox 2 objetos son para redux.
const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingName) => dispatch({type: actionType.ADD_INGREDIENT, ingredientName: ingName}),
        onIngredientRemoved: (ingName) => dispatch({type: actionType.REMOVE_INGREDIENT, ingredientName: ingName})
    };
}

 export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));
// export default withErrorHandler(BurgerBuilder, axios); [TUVE QUE DEJAR EL EXPORT NORMAL, PORQUE EN LA RUTA DEL APP.JS EL BURGERBUILDER NO RECIBIA LOS PROPS]