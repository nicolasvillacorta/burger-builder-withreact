import React from 'react';
import styles from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger = (props) => {
    console.log(props);
    

    let transformedIngredients = Object.keys(props.ingredients) // Object.keys es de JS, no de React, extrae las keys de un objeto y las pone en un array.
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            });
        })
        .reduce((arr, el) => {
            return arr.concat(el) // Empieza con un [] (array vacio) y va concatenando, y asi reduce hasta un array vacio los 4 arrays vacios que tenia
        }, []);
    
    if(transformedIngredients.length === 0){
        transformedIngredients = <p>No le pusiste nada rey!</p>
    } 

    return(
        
        <div className={styles.Burger}>
           <BurgerIngredient type="bread-top" />
           {transformedIngredients}
           <BurgerIngredient type="bread-bottom" />
        </div>


    );


}

export default burger;
// export default withRouter(burger); Wrapeando con este componente, puedo hacer que burger reciba las props de match del router anterior, por default
//      el router le da las props al burgerbuilder, pero este a su vez no las pasa a este, con este componente si.