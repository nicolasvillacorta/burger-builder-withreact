import React, { Component } from 'react';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../UI/Button/Button';

/* 
    Lo converti en un componente de clase, porque para mejorar la performance podia ser un buen candidato a convertir en PureComponent.
    Use el lifecycle hook "componentWillUpdate" y haciendo logs en consola, vi que agregando ingredientes se re-renderiza este componente, de forma
    innecesaria. Sabiendo eso, me voy al componente padre, Modal y le setie a ese la logica de si deberia updatearse o no.
    Tras haber chequeado el componentWillUpdate, podria haber convertido este denuevo en un componente funcional, pero ya fue, lo deje asi.
*/ 
class OrderSummary extends Component {

    componentDidUpdate(){
        console.log("[OrderSummary] WillUpdate");
        
    }

    render(){
        const ingredientSummary = Object.keys(this.props.ingredients)
        .map(igKey => {
            return (
                <li key={igKey}>
                    <span style={{textTransform: 'capitalize'}}>{igKey}</span>: {this.props.ingredients[igKey]}
                </li>) 
        });

        return (
            <Aux>
                <Button></Button>
                <h3>Your Order</h3>
                <p>A delicious burger with following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to Checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled} >CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued} >CONTINUE</Button>
            </Aux>
        );
    }

    
};

export default OrderSummary;