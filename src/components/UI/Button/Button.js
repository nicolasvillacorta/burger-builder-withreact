import React from 'react';
import styles from './Button.module.css';

const button = (props) => (
    <button
        disabled={props.disabled}
        className={[styles.Button, styles[props.btnType]].join(' ')} // De esta manera encadero que la class sea "Button Danger / Button Success"
        onClick={props.clicked}>{props.children}</button>
);


export default button;