import React from 'react';
import styles from './Spinner.module.css';

// Googlie "CSS Spinners" en google y me salio esta pag: https://projects.lukehaas.me/css-loaders/, clave.
// Cambie el Loader:before y Loader:after background por blanco porque sino aparecia media roto el spinner (con un circulo violeta en el medio). 
const spinner = () => (
    <div className={styles.Loader}>Loading...</div>
);

export default spinner;