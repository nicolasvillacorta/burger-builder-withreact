import React, { Component } from 'react';
import styles from './Modal.module.css';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Backdrop from '../Backdrop/Backdrop';


class Modal extends Component {

    // PureComponent tambien chequearia si modalClosed se cambio. Por eso no lo uso. Seria al pedo.
    shouldComponentUpdate(nextProps, nextState){
        // Si no agregaba esta parte "|| nextProps.children !== this.props.children" 
        // el spinner no se iba a mostrar, porque el componente solo cambia su children, no su show.
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    componentDidUpdate(){
        console.log("[Modal] WillUpdate");
    }

    render(){

        return (
            <Aux>
            <Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
            <div 
                className={styles.Modal}
                style={{
                    transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: this.props.show ? '1' : '0'
                }}>
                {this.props.children}
            </div>
        </Aux>
    );
        
    }

 }

export default Modal;