import React from 'react';
import styles from './Input.module.css';

const input = (props) => {

    let inputElement = null;
    const inputClasses = [styles.InputElement]

    if(props.invalid && props.shouldValidate && props.touched){
        inputClasses.push(styles.Invalid);
    }


    switch(props.elementType){ // Tiene que ser inputtype sin camelcase, porque html ya tiene una prop inputType (funciona igual, pero hay un error en consola si uso camel case.)
        case('input'):
            inputElement = <input 
                className={inputClasses.join(' ')}
                 {...props.elementConfig} 
                 value={props.value} 
                 onChange={props.changed}/>; // Les paso las ...props.elementConfig para copiar los atributos normales de HTML que trae cada uno.
            break;
        case('textarea'):
            inputElement = <textarea 
                className={styles.InputElement} 
                {...props.elementConfig} 
                value={props.value} 
                onChange={props.changed}/>;
            break;
        case('select'):
            inputElement = (
                <select 
                className={styles.InputElement} 
                value={props.value}
                onChange={props.changed}>
                {props.elementConfig.options.map(option => (
                    <option key={option.value} value={option.value}>
                        {option.displayValue}
                    </option>
                ))}

                </select>
            );
            break;
        default:
            inputElement = <input 
                className={styles.InputElement} 
                {...props.elementConfig} value={props.value} />;
    }

    return (
        <div className={styles.Input}>
            <label className={styles.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
    
};

export default input;