import React from 'react';
import styles from './Logo.module.css';
import burgerLogo from '../../assets/images/burger-logo.png';


// El path de la imagen no puede ser un string, porque no va a funcionar, tiene que ser accedido de forma dinamica con las llaves {}.
const logo = (props) => (
    <div className={styles.Logo} style={{height: props.height}} >
        <img src={burgerLogo} alt="MyBurguer" /> 
    </div>
);

export default logo;