import React from 'react';
import styles from './NavigationItem.module.css';
import { NavLink } from 'react-router-dom';

const navigationItem = (props) => (
    <li className={styles.NavigationItem}>
        <NavLink 
            to={props.link} /* Si el link empieza con este string del to, se considera active, por eso tuve que agregar el exact. Para que no este siempre hardcodeado, lo traigo del navigationitemS */ 
            exact={props.exact}
            activeClassName={styles.active} >{props.children}</NavLink>
    </li>
);

export default navigationItem;